import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from "../userContext"
import { Navigate } from "react-router-dom";
import Swal from "sweetalert2"

export default function Login() {
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // Process a fetch request to the corresponding API

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json"
            },
            body: JSON.stringify({
                email : email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // If no user info is found, the "access" property will not be available
            if(typeof data.access !== "undefined"){
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                  icon: 'success',
                  title: 'Login Successful!',
                  text: 'Welcome to Zuitt!',
                })

            }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Authentication Failed!',
                  text: 'Please try again!',
                })


            }
        })

        // setUser({
        //     // Store the email in the localStroge
        //     email: localStorage.setItem("email", email)
        // })

        // Clear input fields after submission
        setEmail('');
        setPassword('');

    }

     // Retrieve user details using its token
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: localStorage.setItem("id", data._id),
                isAdmin: localStorage.setItem("email", data.email),
                email: localStorage.setItem("isAdmin", data.isAdmin)
            })
        })
    }


    return (
        (user.token !== null) ?
            <Navigate to="/courses"/>
        :
        <>
            <h1>Login Page</h1>
            <Form onSubmit={(e) => authenticate(e)} className="my-5">
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email" 
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    required
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            { isActive ? 
                <Button variant="success" type="submit" id="submitBtn" className="mt-3">
                    Login
                </Button>
                : 
                <Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
                    Login
                </Button>
            }

        </Form>
        </>
        
    )
}
