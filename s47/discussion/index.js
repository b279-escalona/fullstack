// DOM -> Document Object Manipulation

// CSS ---> class ; JavaScript ---> id

const txtFirstName = document.getElementById("txt-first-name");
const spanFullName = document.getElementById("span-full-name");

txtFirstName.addEventListener("keyup",(event) => {
	spanFullName.innerHTML = txtFirstName.value;
})

// EventListener ---> nakikinig sa mga event(any action done)


txtFirstName.addEventListener("keyup",(event) => {
	console.log(event.target);
	console.log(event.target.value);
})