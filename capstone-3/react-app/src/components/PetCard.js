import { Card, Button, Row, Col, Container, Buton} from 'react-bootstrap';

import { useState } from "react"
import {Link} from "react-router-dom"

export default function PetCard({PetProp}) {
    // Checks if props was successfully passed
    console.log(PetProp.name);
    // checks the type of the passed data
    console.log(typeof PetProp);

   // Desturing the courseProp into their own variables
    const { _id, name, description, price, imgSource } = PetProp;

  	return (
        <Container>
            <Row className="mt-5 text-center">
                <Col className="my-5" lg={{ span: 6, offset: 4 }}>
            	    <Card style={{ width: '30rem' }} className="card-pet">
            	      <Card.Img variant="top" src={imgSource} />
            	      <Card.Body className="text-center">
            	        <Card.Title className="view-name">{name}</Card.Title>
            	        <Button className="btn-visit" as={Link} to={`/petView/${_id}`}>Visit Doggo</Button>
            	      </Card.Body>
            	    </Card>
                </Col>
            </Row>
        </Container>

	);
}

