// object destructuring
import { Row, Col, Button, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Banner({bannerProps}){
	console.log(bannerProps);

	const {title, content, destination, label} = bannerProps;

	return(
		<Container className="homeBanner w-100">
			<Row>
				<Col className="p-5 text-center " lg={12} >
					<h1 className="title-banner">{title}</h1>
					<p className="subtitle-banner">{content}</p>
					<Button as={Link} to={destination} className="btn-banner">{label}</Button>
				</Col>
			</Row>
		</Container>
	);
}