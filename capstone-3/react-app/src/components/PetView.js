import {useState, useEffect, useContext} from "react"
import {Container, Card, Button, Row, Col} from "react-bootstrap"
import { useParams } from "react-router-dom"
import UserContext from "../UserContext"
import Swal from "sweetalert2"

export default function CourseView(){

	const { user } = useContext(UserContext);

	// module that allows us to retrieve the courseId passed via URL
	const { petId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [imgSource, setImgSource] = useState("");

	useEffect(() => {
		console.log(petId);

		fetch(`${process.env.REACT_APP_API_URL}/pets/${petId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImgSource(data.imgSource);
		})

	}, [petId])


	const checkout = (petId) => {
		if(localStorage.getItem("isAdmin") === "true"){
			Swal.fire({
				title: "OOPS!",
				icon: 'error',
				text: "An admin cannot do this action."
			});

			console.log(localStorage.getItem("isAdmin"));
		} else {
			fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
				method: "POST",
				headers: {
					"Content-type" : "application/json",
					Authorization : `Bearer ${localStorage.getItem("token")}`
				},
				body: JSON.stringify({
					petId: petId
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);


				if(data === true){
					Swal.fire({
						title: "Purchased!!",
						icon: 'success',
						text: "New pet owned!"
					});

				}else{
					Swal.fire({
						title: "Something Went Wrong!",
						icon: 'error',
						text: "Purchase unsucessful!"
					});
				}
			})
		}
	}


	return(
		<Container className="my-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className="buy-card">
						<Card.Body className="text-center">
							<Card.Img variant="top" src={imgSource} />
							<Card.Title className="mt-5 font1">{name}</Card.Title>
							<Card.Subtitle className="font2">Description:</Card.Subtitle>
							<Card.Text className="font3">{description}</Card.Text>
							<Card.Subtitle className="font4">Price:</Card.Subtitle>
							<Card.Text className="font5">PhP {price}</Card.Text>
							<Button className="btn-buy" onClick={() => checkout(petId)}>Buy this one</Button>
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>
		);
}



