import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Row, Col } from "react-bootstrap";
import { Link } from "react-router-dom";


export default function FeaturedPets() {
    return (
        <div className=' d-flex flex-column my-5 banner-bg p-5 text-dark '>
        <h2 className ="feat-pets">Top Dogs!</h2>
        <Row className="my-3 text-dark h-100">
            <Col className="my-2 text-center" xs={12} md={4} lg={4}>
                <Card className="card-height" >
                <Card.Img className="p-3 img-crd" variant="top" src={require('../images/soba.jpg')} />
                <Card.Header>
                <Card.Title className="crd-title">Soba</Card.Title>
                </Card.Header>
                <Card.Body>
                    <Card.Text className="crd-subtitle">
                    Soba The Super Cute Toy Poodle
                    </Card.Text>
                </Card.Body>
                <Card.Footer className='text-center'>
                <Button className="w-50 btn-feat text-dark" as = {Link} to={"/pets"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>

            <Col className="my-2 text-center" xs={12} md={4} lg={4}>
                <Card className="card-height">
                <Card.Img className="p-3 img-crd" variant="top" src={require('../images/frankie.jpg')} />
                <Card.Header>
                    <Card.Title className="crd-title">Frankie</Card.Title>
                    </Card.Header>
                <Card.Body>
                    <Card.Text className="crd-subtitle">
                    The Bravest and The Entertainer 
                    </Card.Text>
                </Card.Body>
                <Card.Footer className='text-center'>
                <Button className="w-50 btn-feat text-dark" as = {Link} to={"/pets"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>

            <Col className="my-2 text-center" xs={12} md={4} lg={4}>
                <Card className="card-height">
                <Card.Img className="p-3 img-crd" variant="top" src={require('../images/bonita.jpg')} />
                <Card.Header>
                    <Card.Title className="crd-title">Bonita</Card.Title>
                    </Card.Header>
                <Card.Body>
                    <Card.Text className="crd-subtitle">
                    The Playful One
                    </Card.Text>
                </Card.Body>
                <Card.Footer className='text-center'>
                <Button className="w-50 btn-feat text-dark" as = {Link} to={"/pets"} >View</Button>
                </Card.Footer>
                </Card>
            </Col>
        </Row>
        </div>
    );
}
