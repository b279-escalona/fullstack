import {Nav, Navbar} from "react-bootstrap"
import { Link, NavLink } from "react-router-dom";
import { useState, useContext, useEffect } from "react"
import UserContext from "../UserContext"

export default function AppNavbar(){

  // State to store user info stored in the login page
  /*const [user, setUser] = useState(localStorage.getItem("email"));
  console.log(user);*/

  const { user } = useContext(UserContext);




  return(
  <Navbar expand="lg" className="navBar px-3">
      <Navbar.Brand as={Link} to={"/"} className="fw-bold">Go.Grow.Glow</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ms-auto">

          {
              (localStorage.getItem("isAdmin") === "true")
              ?
              <Nav.Link as={ NavLink } to="/adminPage" end>Admin Page</Nav.Link>
              :
              <>

              
              </>
          }
          {
              (user.token !== null)
              ?
                  <>
                  <Nav.Link as={ NavLink } to="/" end>Home</Nav.Link>
                  <Nav.Link as={ NavLink } to="/pets" end>Pets</Nav.Link>
                  <Nav.Link as={ NavLink } to="/logout" end>Logout</Nav.Link>
                  </>
              :
              <>
                  <Nav.Link as={ NavLink } to="/login" end>Login</Nav.Link>
                  <Nav.Link as={ NavLink } to="/register" end>Register</Nav.Link>
              </>
          }

        </Nav>
      </Navbar.Collapse>
  </Navbar>
    )
}


