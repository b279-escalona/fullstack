import './App.css';

import Register from "./pages/Register"
import Login from "./pages/Login"
import Logout from "./pages/Logout"
import Home from "./pages/Home"
import AppNavbar from "./components/AppNavbar"
import AllProducts from "./pages/AllProducts"
import Pets from "./pages/Pets"
import PetView from "./components/PetView"


// npm install react-router-dom
import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react"
import { UserProvider } from "./UserContext"

// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process
// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page

function App() {

  // Creating a user state for global scope

  /*const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })*/

  const [user, setUser] = useState({
        id: null,
        isAdmin: null,
        email: null,
        token: localStorage.getItem("token")
    })

  // Function for clearing the storage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])



  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
          <Container>
              <Routes>
                  <Route path="/" element={<Home/>}/>
                  <Route path="/register" element={<Register/>}/>
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/logout" element={<Logout/>}/>
                  <Route path="/adminPage" element={<AllProducts/>}/>
                  <Route path="/pets" element={<Pets/>}/>
                  <Route path="/petView/:petId" element={<PetView/>}/>
              </Routes>
          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
