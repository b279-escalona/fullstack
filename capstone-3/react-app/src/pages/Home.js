import Banner from "../components/Banner"
import FeaturedPets from "../components/FeaturedPets"
import Footer from "../components/Footer"



export default function Home(){

	const data = {
	title: "Go. Grow. Growl.",
	content: "Finding fur-ever home with a cost",
	destination: "/pets",
	label: "Buy Now!"
	}

	return(
			<div>
				<Banner bannerProps={data} />
				<FeaturedPets/>
				<Footer className="w-100"/>
			</div>
		)
}