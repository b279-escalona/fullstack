// import courseData from "../data/courses"
import PetCard from '../components/PetCard'
import { useEffect, useState} from "react"


export default function Pets(){


	const [activePets, setAllActivePets] = useState([]);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/pets/active`)
		.then(res => res.json())
		.then(data => {
			console.log(data);


			setAllActivePets(data.map(pets => {
				return (
					<PetCard key={pets._id} PetProp={pets}/>
					)
			}))

		})
	}, [])


	return(

		<div className = "bg-petview">
			<h1 className="mt-5 p-5 text-center">Available Pets</h1>
			{/*Prop making ang prop passing*/}
			{activePets}
		</div>

	)
}