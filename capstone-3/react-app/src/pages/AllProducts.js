import { useContext, useState, useEffect } from "react"
import UserContext from "../UserContext";
import {Navigate} from "react-router-dom";
import {Container, Col, Row, Button, Table, Modal, Form, Image} from "react-bootstrap"
import Swal from "sweetalert2";
// import AppSideNav from "../components/AppSideNav";
import React from "react";
// import logo from "../images/logo.jpeg"
// import HomeBanner from "../components/HomeBanner";


export default function AllProducts(){


    const { user, setUser } = useContext(UserContext);



    const [allProducts, setAllProducts]= useState([]);

    const [productId, setProductId] = useState("");
    const [productName, setProductName] = useState("");
    const [productBrand, setProductBrand] = useState("");
    const [productType, setProductType] = useState("");
    const [productModel, setProductModel] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [imgSource, setImgSource] = useState("");

    const [isActive, setIsActive] = useState(false);

    const [modalAdd, setModalAdd] = useState(false);
    const [modalEdit, setModalEdit] = useState(false);


    const addProductOpen = () => setModalAdd(true);
    const addProductClose = () => setModalAdd(false);

    const openEdit = (id) => {
        setProductId(id);
        console.log(id)

        fetch(`${process.env.REACT_APP_API_URL}/pets/${id}`)
            .then(res => res.json())
            .then(data => {

                console.log(data);

                setProductName(data.name);
                setDescription(data.description);
                setPrice(data.price);
                setImgSource(data.imgSource);
            });

     setModalEdit(true)
    };

    const closeEdit = () => {

        setProductName('');
        setDescription('');
        setImgSource('');
        setPrice(0);
        

     setModalEdit(false);
    };




    /* GET ALL PRODUCTS */
	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/pets/products`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

            setAllProducts(data.map(products =>{
                return (
                    <>
                    
                    <tr key={products._id}>
                        <td>{products._id}</td>
                        <td>{products.name}</td>
                        <td>{products.description}</td>
                        <td>{products.price}</td>
                        <td>{products.isActive ? "Active" : "Inactive"}</td>
                        <td>
                            {(products.isActive)
                            ?
                            <Button variant="danger" size="sm" onClick={() => archive(products._id, products.name)}>Archive</Button>
                            :
                            <>
                            <Button variant="success" className="mx-1" size="sm" onClick={() => unarchive(products._id, products.name)}>Unarchive</Button>
                            <Button variant="secondary" className="mx-1" size="sm" onClick={() => openEdit(products._id)}   >Edit</Button>

                            </>}
                        </td>

                    </tr>
                    </>
                )
            }));
		});
	}


    /* ARCHIVE PRODUCT */
    const archive = (id, name) =>{
        console.log(id);
        console.log(name);
        fetch(`${process.env.REACT_APP_API_URL}/pets/${id}/archive`,
        {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: false
        })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "PRODUCT ARCHIVING SUCCESS!",
                    icon: "success",
                    text: `The product is now in archive`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Archive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

    const unarchive = (id, name) =>{
        console.log(id);
        console.log(name);
        fetch(`${process.env.REACT_APP_API_URL}/pets/${id}/archive`,
        {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        body: JSON.stringify({
            isActive: true
        })
    
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data){
                Swal.fire({
                    title: "Unarchive Successful",
                    icon: "success",
                    text: `The product is now inactive`
                });

                fetchData();
            }else{
                Swal.fire({
                    title: "Unarchive Unsuccessful",
                    icon: "error",
                    text: "Something went wrong. Please try again later"
                })
            }
        })
    }

	useEffect(()=>{
		fetchData();


	}, [])

    /* ADD PRODUCT */
    const addProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/pets/products`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: productName,
                description: description,
                price: price,
                imgSource: imgSource
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "PRODUCT ADDED SUCCESSFULLY!",
                        icon: "success",
                        text: `"The new product was added to the product list.`,
                    });


                    fetchData();
                    addProductClose();
                }
                else {
                    Swal.fire({
                        title: "ADD PRODUCT UNSSUCCESSFUL!",
                        icon: "error",
                        text: `The system is experiencing trouble at the moment. Please try again later.`,
                    });
                    addProductClose();
                }

            })
        setProductBrand('');
        setProductType('');
        setProductModel('');
        setProductName('');
        setImgSource('');
        setDescription('');
        setPrice(0);
        
    }

    /* EDIT PRODUCT */
    const editProduct = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/pets/${productId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: productName,
                description: description,
                price: price,
                imgSource: imgSource
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if (data) {
                    Swal.fire({
                        title: "PRODUCT EDIT SUCCESSFUL!",
                        icon: "success",
                        text: `Product was edited successfully.`,
                    });

                    fetchData();
                    closeEdit();

                }
                else {
                    Swal.fire({
                        title: "PRODUCT EDIT UNSUCCESSFUL!",
                        icon: "error",
                        text: `The system is experiencing trouble at the moment. Please try again later.`,
                    });

                    closeEdit();
                }

            })

        setProductName('');
        setDescription('');
        setPrice(0);
        
    }

    useEffect(() => {

        if (productName != "" && description != "" && price > 0) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [productName, description, price]);




	return (

            (localStorage.getItem("isAdmin") === "true") ? 
    
            <>

            

            
                <Container fluid>
                    <Row>
                        <Col>
                            <div className="my-2 text-center">
                                <h1>VIEW ALL PRODUCTS</h1>
                            </div>

                            <div className="my-2 text-right">
                                <Button  variant="danger" className="px-5" onClick={addProductOpen}>ADD A PRODUCT</Button>
                            </div>

                            <Table striped bordered hover>
                                <thead className="banner-bg text-dark text-left">
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th className="w-25">Description</th>
                                        <th>Price</th>
                                        <th>Stocks</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            <tbody>
                                {allProducts}
                            </tbody>
                            </Table>
                        </Col>
                    </Row> 
                </Container>

            <div className="w-100 bg-white rounded p-5">
                     {/* ADD PRODUCT MODAL */}

                    <Modal
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    show={modalAdd}
                    >
                    <Form onSubmit={e => addProduct(e)}>

                            <Modal.Header className="banner-bg text-light">
                                <Modal.Title>ADD PRODUCT</Modal.Title>
                            </Modal.Header>

                            <Modal.Body>
                            <Row>
                            <Col xs={6} md={6} lg={6}>
                            <Image className="editModal-img-fit mb-2 container-fluid"
                                    src={imgSource}
                                />
                                
                                <Form.Group controlId="ProductName" className="mb-3">
                                    <Form.Label>Pet Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Product Name"
                                        value={productName}
                                        onChange={e => setProductName(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                                </Col>

                                <Col xs={6} md={6} lg={6}>
                                <Form.Group controlId="description" className="mb-3">
                                    <Form.Label>Pet Description</Form.Label>
                                    <Form.Control
                                        as="textarea"
                                        rows={3}
                                        placeholder="Product Description"
                                        value={description}
                                        onChange={e => setDescription(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="price" className="mb-3">
                                    <Form.Label>Pet Price</Form.Label>
                                    <Form.Control
                                        type="number"
                                        placeholder="Product Price"
                                        value={price}
                                        onChange={e => setPrice(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="imgSource" className="mb-3">
                                    <Form.Label>Image Link</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Product Image Link"
                                        value={imgSource}
                                        onChange={e => setImgSource(e.target.value)}
                                        required
                                    />
                               
                                </Form.Group>
                                </Col>
                                </Row>
                            </Modal.Body>

                            <Modal.Footer>
                                {isActive
                                    ?
                                    <Button variant="primary" type="submit" id="submitBtn">
                                        ADD PRODUCT
                                    </Button>
                                    :
                                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                                        ADD PRODUCT
                                    </Button>
                                }
                                <Button variant="secondary" onClick={addProductClose}>
                                    Close
                                </Button>
                            </Modal.Footer>

                        </Form>
                    </Modal>
                    
                    {/* EDIT MODAL */}

                    <Modal
                    size="lg"
                    aria-labelledby="contained-modal-title-vcenter"
                    centered
                    show={modalEdit}
                    >
                    <Form onSubmit={e => editProduct(e)}>

                            <Modal.Header className="banner-bg text-light">
                                <Modal.Title>EDIT PRODUCT</Modal.Title>
                            </Modal.Header>

                            <Modal.Body>
                                <Image className="container-fluid mb-2"
                                    src={imgSource}
                                />
                                <Form.Group controlId="productName" className="mb-3">
                                    <Form.Label>Product Name</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Enter Product Name"
                                        value={productName}
                                        onChange={e => setProductName(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="description" className="mb-3">
                                    <Form.Label>Product Description</Form.Label>
                                    <Form.Control
                                        as="textarea"
                                        rows={3}
                                        placeholder="Enter Product Description"
                                        value={description}
                                        onChange={e => setDescription(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="price" className="mb-3">
                                    <Form.Label>Product Price</Form.Label>
                                    <Form.Control
                                        type="number"
                                        placeholder="Enter Product Price"
                                        value={price}
                                        onChange={e => setPrice(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="imgSource" className="mb-3">
                                    <Form.Label>Image Link</Form.Label>
                                    <Form.Control
                                        type="text"
                                        placeholder="Product Image Link"
                                        value={imgSource}
                                        onChange={e => setImgSource(e.target.value)}
                                        required
                                    />
                               
                                </Form.Group>
                                
                            </Modal.Body>

                            <Modal.Footer>
                                {isActive
                                    ?
                                    <Button variant="primary" type="submit" id="submitBtn">
                                        Save
                                    </Button>
                                    :
                                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                                        Save
                                    </Button>
                                }
                                <Button variant="secondary" onClick={closeEdit}>
                                    Close
                                </Button>
                            </Modal.Footer>

                        </Form>
                    </Modal>



            </div>

            


        </>
        :
        <Navigate to = "/"/>

        )

}